<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plan des tables Simplon</title>
    <link rel="stylesheet" media="screen" href="tableSimplon.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
    
<?php

$prenoms = ['Esma', 'Esther', 'Laurie', 'Marion', 'Grace', 'Nathalie', 'Hayette', 'Isabelle', 'Otilia', 'Viviane', 'Maude', 'Léa', 'Florence', 'Sixtine', 'Fatima', 'Syriane'];

/**
  * Form group / table in a random way
 *
 * @param array $nameArray
 *      Array of all names 
 * @param int $nbrPerTable
 *      number of person per table / group to make
 * @return string
 *      Nom du fichier calculé
**/
function setTable ($nameArray, $nbrPerTable) {
    $nbrTable = count($nameArray) / $nbrPerTable;
    for ($i=1; $i <= $nbrTable; $i++) {
        $table = [];
        //get $nbrPerTable names
        $randomKeys = array_rand($nameArray, $nbrPerTable);
        //add thoses names in a table
        foreach($randomKeys as $key){
            $table[] = $nameArray[$key];
        }
        //suppress names in original array
        $nameArray = array_diff ($nameArray, $table);
        //show in html the table
        echo("<div><h2>Table $i</h2>");
        foreach ($table as $name) {
        echo("<p>$name</p>");
        }
        echo('</div>');
    }    
}

setTable($prenoms, 4);

?>
</body>
</html>